
#include <cstdio>
#include <vector>
 
using namespace std;
 
 // negatives_before_positives_ will is class which takes care of the logic.
void negatives_before_positives_(vector<int> &v)
{
    int position_of_very_first_element = -1; 
 
    for (int i=0; i<(int)v.size(); ++i)
    {
        if (v[i] >= 0)
        {
            if (position_of_very_first_element < 0)
                position_of_very_first_element = i;
        }
        else
        {
            if (position_of_very_first_element >= 0)
            {
                std::swap(v[position_of_very_first_element], v[i]);
                position_of_very_first_element++;
 
                
                for (int j=i-1; j>= position_of_very_first_element; --j)
                    std::swap(v[j], v[j+1]);
            }
        }
    }
}
 
 // Testing out the main fucntion with negatives_before_positives_
int main()
{
    vector<int> v{12, 11, -13, -5, 6, -7, -3, 6};
    negatives_before_positives_(v);
    for (int el : v) printf("%d\n", el);
}