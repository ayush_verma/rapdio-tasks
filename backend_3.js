
function CountTheNumberofVotes(votes) {
  var candidatesInCity = [];

  for (var i = 0; i < votes.length; i++) {
    if (!candidatesInCity.some(element => element.name === votes[i])) {
      candidatesInCity.push({
        name: votes[i],
        count: 1,
      });
    } else {
      candidatesInCity[candidatesInCity.findIndex(cand => cand.name === votes[i])].count++;
    }
  }
  //Our logic comes into play here
  var maxVotes = Math.max.apply(Math, candidatesInCity.map(cand => cand.count));

  var winners = candidatesInCity.filter(candi => candi.count === maxVotes);
  
  return winners
    .sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    })
    .map(winner => winner.name)
    .shift();
}

console.log(CountTheNumberofVotes(['Alex', 'Michael', 'Harry', 'Dave', 'Michael', 'Victor', 'Harry', 'Alex', 'Mary', 'Mary']));
